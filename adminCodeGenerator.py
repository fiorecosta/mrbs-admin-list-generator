# MRBS Admin User Generator
# FioreCosta 2019
# Auto Generate user tags for ldap admin accounts

##import modules
import csv

### SETTINGS

#CSV file to import - place file in same location as python script
fileName = "file.csv"

#User Level to be used by script
userLevel =  "admin"

#csv column(col) number in file containing login account information
colN = 1

#Remove Domain Text
#Change to True if csv file contains email address for username
# If changed to true replace with domain text to replace
domainText = False                      
domain = "@domain.com"                 

### Main Script to add correct admin text

with open(fileName) as csvFile:
    csvFile = csv.reader(csvFile, delimiter=',')
    lineCount = 0
    for col in csvFile:
        list = ('$auth["{0}"][] = "{1}";' .format(userLevel, col[colN]))

        if domainText == True:
            print(list.replace(domain, ""))
        else:
            print(list)